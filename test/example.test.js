// test/example.test.js

const expect = require('chai').expect;
const should = require('chai').should();
const { assert } = require('chai');
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {

    let myvar = undefined;

    before(() => {
        myvar = 1; // setup before testing
        console.log('before testing...');
    })

    it('Myvar should exist', () => {
        should.exist(myvar);
    })

    it('Should return 4 when using sum function with a=2, b=2', () => {
        const result = mylib.sum(2,2);  // 2 + 2
        expect(result).to.equal(4); // result expected to be equal 4
    });

    it('parametrized way of unit testin', () => {
        const result = mylib.sum(myvar, myvar); // 2 + 2
        expect(result).to.equal(myvar+myvar);
    })

    it('Assert foo is not bar', () => {
        assert('foo' !== 'bar') // true
    })

    after(() => {
        console.log('after testing..');
    })

})